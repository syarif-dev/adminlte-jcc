@extends('adminlte.master')

@section('title', 'Home')

@section('card-title','Home')

@section('content')
<h1>SanberBook</h1>

<h3>Sosial Media Developer</h3>

<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

<h3>Benefit Join di SanberBook</h3>

<ul>
    <li>Mendapatkan motivasi dari sesama developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>

<h3>Cara Bergabung di SanberBook</h3>

<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="{{route("register")}}">Form Sign Up</a></li>
    <li>Selesai</li>
</ol>
@endsection
