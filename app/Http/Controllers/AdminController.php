<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function master(){
        return view('adminlte.master');
    }

    public function table(){
        return view('tables.table');
    }

    public function datatables(){
        return view('tables.dataTable');
    }
}
