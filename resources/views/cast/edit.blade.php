@extends('adminlte.form-master')

@section('title', 'Edit')

@section('card-title')
<h3 class="card-title">Edit Cast {{ $cast->id }}</h3>
@endsection

@section('content')
<form action="/cast/{{ $cast->id }}" method="POST" id="form">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="nama">Nama</label>
            <input
                type="text"
                class="form-control"
                name="nama"
                id="nama"
                placeholder="Masukkan Nama"
                value="{{ old('nama', $cast->nama) }}"
                required
            />
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input
                type="number"
                class="form-control"
                name="umur"
                id="umur"
                placeholder="Masukkan Umur(tahun)"
                value="{{ old('umur', $cast->umur) }}"
                required
            />
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea
                type="text"
                class="form-control"
                name="bio"
                id="bio"
                placeholder="Masukkan Bio"
                cols="30"
                rows="10"
                required
            >{{ old('bio', $cast->bio) }}</textarea>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>

@endsection
