@extends('adminlte.form-master')

@section('title', 'Cast')

@section('card-title')
<h3 class="card-title">Cast</h3>
@endsection

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush

@section('content')

@if(session('success'))

<div class="alert alert-success my-2 mx-2">
    {{ session('success') }}
</div>

@endif
<div>
    <a class="btn btn-primary ml-2 my-2" href="{{ route('cast-create') }}">Add New Cast</a>
</div>
<div class="mx-2">
    <table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>No.</th>
            <th>Nama </th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item )
        <tr>
            <td style="width: 5%">{{ $key + 1 }}</td>
            <td style="width: 25%">{{ $item->nama }}</td>
            <td style="width: 15%">{{ $item->umur }}</td>
            <td style="width: 35%">{{ $item->bio }}</td>
            <td style="width: 20%">
                <a class="btn btn-info btn-sm" href="/cast/{{ $item->id }}">Detail</a>
                <a class="btn btn-warning btn-sm" href="/cast/{{ $item->id }}/edit">Edit</a>
                <form action="/cast/{{ $item->id }}" method="POST" style="display: inline">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <h2 class="ml-2">No Cast Found</h2>
        @endforelse
    </tbody>
    <tfoot>
        <tr>
            <th>No.</th>
            <th>Nama </th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
        </tr>
    </tfoot>
</table>
</div>


@endsection

@push('scripts')
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush
