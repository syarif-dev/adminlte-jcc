<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('pages.register');
    }

    public function welcome(Request $request){
        // dd($request->all());
        $firstName = $request['firstName'];
        $lastName = $request['lastName'];
        $fullName = ucwords($firstName." ".$lastName);
        return view('pages.welcome', compact('fullName'));
    }

}
