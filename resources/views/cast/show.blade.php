@extends('adminlte.form-master')

@section('title', 'Detail')

@section('card-title')
<h3 class="card-title">Detail Cast {{ $cast->id }}</h3>
@endsection

@section('content')
<div>
    <a class="btn btn-secondary ml-2 my-2" href="{{ route('cast-index') }}">Back</a>
</div>
<div class="flex flex-col justify-center mx-2">
    <div>
        <h1 class="text-center font-bold my-8 text-xl ">
            Detail of {{ $cast->nama }}
        </h1>
    </div>
    <div class="bg-gray-100 shadow-xl p-12 mx-6 my-6">
        <p class="m-6 text-2xl font-bold  ">
            Nama : {{ $cast->nama }}
        </p>
        <p class="m-6 text-2xl font-bold  ">
            Umur : {{ $cast->umur }}
        </p>
        <p class="m-6 text-2xl font-bold  ">
            Bio : {{ $cast->bio }}
        </p>
    </div>
</div>

@endsection
