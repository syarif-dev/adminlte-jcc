<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = DB::table('casts')->get();
        return view('cast.index', compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "nama" => 'required|unique:casts',
            "umur" => 'required',
            "bio" => 'required'
        ],[
            "nama.required" => "Nama harus diisi",
            "umur.required" => "Umur harus diisi",
            "bio.required" => "Bio harus diisi"
        ]);

        DB::table('casts')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast')->with('success', 'Cast Berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $request->validate([
            "nama" => 'required',
            "umur" => 'required',
            "bio" => 'required'
        ],[
            "nama.required" => "Nama harus diisi",
            "umur.required" => "Umur harus diisi",
            "bio.required" => "Bio harus diisi"
        ]);

        DB::table('casts')
                ->where('id', $id)
                ->update([
                'nama'=> $request['nama'],
                'umur'=> $request['umur'],
                'bio'=> $request['bio']
        ]);

        return redirect('/cast')->with('success', 'Berhasil Update Cast!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast')->with('success', "Berhasil Delete Cast!");
    }
}
