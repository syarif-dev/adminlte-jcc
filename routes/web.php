<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AdminController;
// Route Home
Route::get('/', 'HomeController@index')->name('home');

// Route register
Route::get('/register', 'AuthController@index')->name('register');
Route::post('/welcome', 'AuthController@welcome')->name('welcome');

Route::get('/master', 'AdminController@master')->name("master");
// Route Table
Route::get('/table', 'AdminController@table')->name("table");
Route::get('/data-tables', 'AdminController@datatables')->name("dataTables");

// Route Cast
Route::get('/cast', 'CastController@index')->name('cast-index');
Route::get('/cast/create', 'CastController@create')->name('cast-create');
Route::post('/cast', 'CastController@store')->name('cast-store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update')->name('cast-update');
Route::delete('/cast/{cast_id}', 'CastController@destroy')->name('cast-destroy');
